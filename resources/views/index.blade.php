<html>
    <head>
        <meta content="text/html; charset=UTF-8">
        <link href="{{url('/assets/css/styles.css')}}" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>
        <script type="text/javascript">
            function make_move (_cell_) {
                var cellId = $(_cell_).attr("value");
                $.post('/battleship/public/api/v1/make_move/{{$boardId}}/'+cellId+'?_token='+$('input[name=_token]').val(), function ( data) {
                    var response = jQuery.parseJSON(data);
                    if (response.status == 'miss') {
                        $(_cell_).append('X');

                        /*Update total number of miss*/
                        var miss = $('#scoreBoard').find('tr:first-child>td:first-child').text();
                        var totalMiss = parseInt(miss)+1;
                        $('#scoreBoard').find('tr:first-child>td:first-child').text(totalMiss);
                    } else if (response.status == 'hit') {
                        $(_cell_).attr('style', 'background-color: green');

                        /*Update total number of hits*/
                        var hits = $('#scoreBoard').find('tr:nth-child(2)>td:first-child').text();
                        var totalHits = parseInt(hits)+1;
                        $('#scoreBoard').find('tr:nth-child(2)>td:first-child').text(totalHits);
                    }

                    /*Update total number of moves*/
                    var totalMoves =  parseInt($('#scoreBoard').find('tr:nth-child(2)>td:first-child').text()) + parseInt($('#scoreBoard').find('tr:first-child>td:first-child').text());
                    $('#scoreBoard').find('tr:nth-child(3)>td:first-child').text(totalMoves);

                    /*Disable consequent clicks*/
                    $(_cell_).removeAttr('onclick');

                    $.get('/battleship/public/web/fetch_ships/{{$boardId}}', function ( data) {
                        var response = jQuery.parseJSON(data);
                        $('#scoreBoard').find('tr:nth-child(4)>td:first-child').text(response.sunk_ships);

                        /*Once all ships sunk, remove onclick attribute to disable API/WEB call function*/
                        if (response.sunk_ships == {{count($ships)}}) {
                            $("td").each(function() {
                                $(this).removeAttr('onclick');
                            });
                            alert('All ships have sunk. Thank you for playing!');
                        }
                    });
                });

            }
        </script>
    </head>
    <body>
        {{csrf_field()}}

        <div style="float: left">
            {!! $board !!}
        </div>

        <div style="float: right">
            <h1>LIVE SCORE</h1>
            <table style='font-size: 25px;border-spacing: 5px; ' id="scoreBoard">
                <tr>
                    <td style='border: 1px solid black; padding: 5px'>0</td><td>Miss</td>
                </tr>
                <tr>
                    <td style='border: 1px solid black; padding: 5px'>0</td><td>Hits</td>
                </tr>
                <tr>
                    <td style='border: 1px solid black; padding: 5px'>0</td><td>Moves</td>
                </tr>
                <tr>
                    <td style='border: 1px solid black; padding: 5px'>0</td>
                    <td>out of {{count($ships)}} SHIPS SUNK</td>
                </tr>
            </table>
        </div>
    </body>
</html>
