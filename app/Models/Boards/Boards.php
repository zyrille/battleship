<?php  namespace App\Models\Boards;

use Illuminate\Database\Eloquent\Model;

class Boards extends Model
{
    protected $table = 'boards';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cells', 'ships', 'miss', 'hits'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];


}
