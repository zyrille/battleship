<?php  namespace App\Models\Ships;

use Illuminate\Database\Eloquent\Model;

class Ships extends Model
{
    protected $table = 'ships';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'ship_size'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];


}
