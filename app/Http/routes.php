<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/start', 'BattleShipController@start');

/*WEB*/
Route::get('/web/game', 'BattleShipController@create_game');
Route::get('/web/fetch_ships/{boardId?}', 'BattleShipController@fetch_sunk_ships');

/*API*/
Route::post('/api/v1/create_game', 'ApiController@create_game');
Route::post('/api/v1/make_move/{boardId?}/{cellId?}', 'ApiController@make_move');
Route::get ('/api/v1/boards/{boardId?}', 'ApiController@get_board_state');