<?php
namespace App\Http\Controllers;

use App\Models\Boards\Boards;
use App\Models\Ships\Ships;
use Illuminate\Http\Request;

class BattleShipController extends Controller  {

    public function create_game ()
    {
        $ships = self::get_ships();
        $deployedShips = self::deploy_ships($ships);
        if ($deployedShips['success']) {
            $boardId = self::save_board($deployedShips['ships']);
            $board = self::construct_table();
        } else {
            return redirect('/web/game');
        }

        return view('index')
            ->with('boardId', $boardId)
            ->with('ships', $ships)
            ->with('board', $board);
    }

    public function get_ships ()
    {
        $ships = new Ships();
        $shipsObj = $ships::where('id', '!=', 0)->get();

        return $shipsObj;
    }

    public function deploy_ships ($ships)
    {
        $deploy = array();
        $deploy['occupiedCells'] = array();
        $success = 1;

        foreach ($ships as $ship) {
            $setter = rand(1,2);
            $cellLimit = 100;
            for ($cell = 1; $cell <= $ship->ship_size; $cell++) {
                $ship_id = $ship->id;
                if ($setter == 1) { /*We're placing the ship Horizontally!*/
                    if ($cell == 1) {
                        while( in_array( ($cellNo = rand(1,(100 - ($ship->ship_size - 1)))), $deploy['occupiedCells'] ) );
                        if ($cellNo <= 10) {
                            $cellLimit = 10;
                        } elseif ($cellNo > 10 && $cellNo <= 20) {
                            $cellLimit = 20;
                        } else {
                            if (substr($cellNo, -1) == "0") {
                                $cellLimit = $cellNo;
                            } else {
                                $cellLimit = (substr($cellNo, 0, 1) + 1) . "0";
                            }
                        }
                    } else {
                        $cellNo = $deploy['shipArray'][$ship_id][$cell-1] + 1;
                        if (in_array($cellNo, $deploy['occupiedCells']) ||
                            $cellNo > $cellLimit) {
                            return array("ships" => $deploy, "cell" => $cellNo, "limit" => $cellLimit, "success" => 0);
                        }
                    }
                } else { /*We're placing the ship Vertically!*/
                    if ($cell == 1) {
                        while( in_array( ($cellNo = rand(1,(((10 - ($ship->ship_size)) * 10) + 10))), $deploy['occupiedCells']) );
                    } else {
                        $cellNo = $deploy['shipArray'][$ship_id][$cell-1] + 10;
                        if (in_array($cellNo, $deploy['occupiedCells']) ||
                            $cellNo > $cellLimit) {
                            return array("ships" => $deploy, "cell" => $cellNo, "limit" => $cellLimit, "success" => 0);
                        }
                    }
                }

                $deploy['shipArray'][$ship->id][$cell] = $cellNo;
                
                $deploy['myShips'][$ship_id]['cells'][$cellNo] = '';
                $deploy['myShips'][$ship_id]['sunk'] = 'no';
                
                $deploy['occupiedCells'][] = $cellNo;
            }
        }

        return array("ships" => $deploy, "success" => $success);
    }

    public function save_board ($deployedShips)
    {
        $apiController = new ApiController();
        $data = new \stdClass();
        $cells = array();

        for ($i = 1; $i <= 100; $i++) {
            if (!in_array($i, $deployedShips['occupiedCells'])) {
                $cells[$i] = 'empty';
            } else { $cells[$i] = ''; }
        }

        $data->cells = $cells;
        $data->ships = $deployedShips['myShips'];
        $boardId = $apiController->create_game($data);

        return $boardId;
    }

    public function construct_table ()
    {
        $table = "<table id='gameBoard' style='font-size: 25px;border-spacing: 5px;' >";

        $table .= "<thead>";
        $table .= "<tr>";
        $table .= "<th></th>";
        for ($l = "A"; $l <= "J"; $l++) {
            $table .= "<th>$l </th>";
        }
        $table .= "</thead>";

        $ctr = 1;
        $table .= "<tbody>";
        $table .= "<tr>";
        $table .= "<tr><td>$ctr</td>";

        for ($i = 1; $i <= 100; $i++)
        {
            if ($i % 10 == 0) {
                $ctr++;
                $table .= "<td value=$i onclick='javascript: make_move(this)'></td></tr>";
                if ($ctr <= 10) {
                    $table .= "<tr><td>$ctr</td>";
                }
            } else {
                $table .= "<td value=$i onclick='javascript: make_move(this)'></td>";
            }
        }

        $table .= "</tbody>";
        $table .= "</table>";

        return $table;
    }

    public function fetch_sunk_ships (Request $request)
    {
        $board = new Boards();
        $data = $board::where('id', $request->boardId)
            ->get();

        $ships = unserialize($data[0]->ships);

        $sunk_ships = 0;
        foreach ($ships as $ship_id => $ship) {
            $hit = 0;
            foreach ($ship['cells'] as $cell) {
                if ($cell == 'hit') {
                    $hit += 1;
                }
            }

            if ($hit == (count($ship['cells']))) {
                $sunk_ships += 1;
                $ships[$ship_id]['sunk'] = 'yes';
            }
        }

        $board::where('id', $request->boardId)
            ->update(array('ships' => serialize($ships)));

        return json_encode(array("sunk_ships" => $sunk_ships));
    }
}
