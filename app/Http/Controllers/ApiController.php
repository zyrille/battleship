<?php
namespace App\Http\Controllers;

use App\Models\Boards\Boards;
use Illuminate\Http\Request;

class ApiController extends Controller  {

    /*
     * URL: http://localhost/battleship/api/v1/create_game
     * TYPE: POST
     * */
    public function create_game ($data) {
        $board = new Boards();
        $board->cells = serialize($data->cells);
        $board->ships = serialize($data->ships);
        $board->hits = 0;
        $board->miss = 0;
        $board->save();

        return $board->id;
    }

    /*
     * URL: http://localhost/battleship/api/v1/make_move/$boardId/$cellId
     * TYPE: POST
     * */
    public function make_move (Request $request) {

        $board = new Boards();
        $data = $board::where('id', $request->boardId)
            ->get();

        $ships = unserialize($data[0]->ships);
        $cells = unserialize($data[0]->cells);
        $miss = $data[0]->miss;
        $hits = $data[0]->hits;

        $cells[$request->cellId] = ($cells[$request->cellId] != 'hit' ? 'miss': $cells[$request->cellId]);

        foreach ($ships as $ship_id => $ship) {
            if (in_array($request->cellId, array_keys($ship['cells']))) {
                if ($ships[$ship_id]['cells'][$request->cellId] != 'hit') {
                    $hits = $hits + 1;
                }
                $ships[$ship_id]['cells'][$request->cellId] = 'hit';
                $cells[$request->cellId] = 'hit';
            }
        }

        if ($cells[$request->cellId] == 'miss') {
            $miss = $miss + 1;
        }

        $board::where('id', $request->boardId)
            ->update(array('ships' => serialize($ships), 'hits' => $hits, 'miss' => $miss, 'cells' => serialize($cells)));

        return json_encode(array("status" => $cells[$request->cellId]));

    }

    /*
     * URL: http://localhost/battleship/api/v1/boards/$id
     * TYPE: GET
     * */
    public function get_board_state ($id = 0)
    {
        $boards = new Boards();
        $board = $boards::where('id', $id)->get();

        $data = new \stdClass();
        if (empty($board)) {
            return 'No record found';
        } else {

            $cells = unserialize($board[0]->cells);

            $data->board = $cells;
            $data->ships = unserialize($board[0]->ships);
            $data->moves = $board[0]->hits + $board[0]->miss;

            dd($data);
        }

    }

}
